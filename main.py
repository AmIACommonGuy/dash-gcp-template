import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt
import plotly.graph_objects as go

app = dash.Dash(__name__)

# Import Data
flights = pd.read_csv("flights.csv.gz")
airports = pd.read_csv("airports.csv")

## Graph 1
delay_df = flights.groupby('origin').mean().reset_index()
fig_4=px.bar(delay_df, x="origin", y = 'dep_delay', title="Averaged Departure Delay in EWR, JFK, and LGA",
    labels=dict(origin="Departure Airport", dep_delay="Averaged Departure Delay Time"))

## Graph 2
merged_airport_mean = pd.merge(airports, flights, left_on='faa', right_on='dest')
merged_airport_mean = merged_airport_mean.set_index('origin').groupby(['origin','state']).mean().reset_index()
COLS = 3
ROWS = 1
fig = make_subplots(
    rows=ROWS, cols=COLS,
    subplot_titles = ['EWR', 'JFK', 'LGA'])
results = merged_airport_mean[['state','dep_delay','origin']]
layout = dict(
    title_text = "Averaged Departure Delay across Destination States",
    geo_scope='usa',
)
for i, origin in enumerate(['EWR', 'JFK', 'LGA']):
    geo_key = 'geo'+str(i+1) if i != 0 else 'geo'  
    result = results[['state', 'dep_delay']][results.origin == origin] 
    fig.add_trace(go.Choropleth(
        locations=result.state,
        z = result.dep_delay,
        locationmode = 'USA-states', # set of locations match entries in `locations`
        marker_line_color='white',
        zmin = 0,
        geo=geo_key,
        zmax = max(results['dep_delay']),
        colorbar_title = "Depature Delay",
    ))
    layout[geo_key] = dict(
        scope = 'usa',
        domain = dict( x = [], y = [] ),
    )
z = 0
COLS = 3
ROWS = 1
for y in reversed(range(ROWS)):
    for x in range(COLS):
        geo_key = 'geo'+str(z+1) if z != 0 else 'geo'
        layout[geo_key]['domain']['x'] = [float(x)/float(COLS), float(x+1)/float(COLS)]
        layout[geo_key]['domain']['y'] = [float(y)/float(ROWS), float(y+1)/float(ROWS)]
        z=z+1
        if z > 4:
            break          
fig.update_layout(layout)

## App Layout
app.layout = html.Div(children=[
    html.H1(children='HW6: MY Dash APP'),

    html.Div(children='''
        For this app, I am most interested in the association between the averaged departure delay and geography factors. 
        First I examined the Averaged Departure Delay in EWR, JFK, and LGA.
        It is interesting to find out that EWR has a greater delay value on average.
    '''),


    dcc.Graph(
        id='example-graph2',
        figure=fig_4
    ),

        html.Div(children='''
        Then I use this Choropleth to study the averaged delay across arriving states. It seems that states in the middle of the nations tends to have higher values.
    '''),
    dcc.Graph(
        id='e',
        figure=fig
    )
])


if __name__ == '__main__':
    app.run_server(debug=True, port = 8080)







